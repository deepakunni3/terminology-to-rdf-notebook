# Terminology to RDF notebook

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/deepakunni3%2Fterminology-to-rdf-notebook/HEAD?labpath=External%20terminologies%20to%20RDF.ipynb)

This repository provides a hands on tutorial on how to go about transforming
an external terminology into RDF using Python.

The [External terminologies to RDF.ipynb](External terminologies to RDF.ipynb) notebook provides a step-by-step example of transforming a Pizza vocabulary into an RDF form.

The original Pizza vocabulary is provided as an excel file: [pizza_vocabulary.xlsx](pizza_vocabulary.xlsx)
